FROM centos/python-38-centos7:latest

USER root
RUN yum install -y epel-release
RUN yum update -y && \
    yum install -y \
        binutils \
        build-essential \
        cmake \
        cmake3 \
        cpp \
        dpkg-dev \
        g++ \
        gcc \
        gfal2-all \
        gfal2-util \
        python3-gfal2 \
        gfortran \
        git \
        graphviz-dev \
        libavahi-compat-libdnssd-dev \
        libffi-dev \
        libfftw3-dev \
        libftgl-dev \
        libglew1.5-dev \
        libgsl0-dev \
        libkrb5-dev \
        libldap2-dev \
        libmysqlclient-dev \
        libpcre3-dev \
        libpq-dev \
        libqt4-dev \
        libssl-dev \
        libx11-dev \
        libxext-dev \
        libxext-dev \
        libxft-dev \
        libxml2-dev \
        libxpm-dev \
        libxpm-dev \
        make \
        openssl \
        python3 \
        python3-pip \
        python-dev \
        python-pip \
        xlibmesa-glu-dev \
        zlib1g-dev && \
    yum clean all && \
    rm -rf /var/cache/yum

RUN pip install --upgrade pip

# gfal2-python bindings
RUN curl -o /etc/yum.repos.d/gfal2-repo.repo https://dmc-repo.web.cern.ch/dmc-repo/dmc-el7.repo && \
    git clone --branch v1.12.0 https://github.com/cern-fts/gfal2-python.git && \
    cd gfal2-python/ && \
    ./ci/fedora-packages.sh && \
    cd packaging/ && \
    RPMBUILD_SRC_EXTRA_FLAGS="--without docs --without python2" make srpm && \
    yum-builddep -y python3-gfal2 && \
    pip install gfal2-python

RUN pip3 install rucio-clients==1.29.1

# EGI trust anchors
RUN curl -Lo /etc/yum.repos.d/EGI-trustanchors.repo https://repository.egi.eu/sw/production/cas/1/current/repo-files/EGI-trustanchors.repo && \
    yum -y install ca-certificates ca-policy-egi-core && \
    yum -y update

RUN alternatives --install /usr/local/bin/cmake cmake /usr/bin/cmake 10 \
    --slave /usr/local/bin/ctest ctest /usr/bin/ctest \
    --slave /usr/local/bin/cpack cpack /usr/bin/cpack \
    --slave /usr/local/bin/ccmake ccmake /usr/bin/ccmake \
    --family cmake

RUN alternatives --install /usr/local/bin/cmake cmake /usr/bin/cmake3 20 \
    --slave /usr/local/bin/ctest ctest /usr/bin/ctest3 \
    --slave /usr/local/bin/cpack cpack /usr/bin/cpack3 \
    --slave /usr/local/bin/ccmake ccmake /usr/bin/ccmake3 \
    --family cmake

RUN yum install -y gcc-c++ \
    libX11-devel libXpm-devel libXft-devel libXext-devel python openssl-devel

RUN git clone --branch v6-24-00-patches --depth=1 https://github.com/root-project/root.git root_src
RUN mkdir root_build root_install
WORKDIR root_build
RUN cmake -DCMAKE_INSTALL_PREFIX=../root_install ../root_src
RUN cmake --build . -- install -j4
RUN source ../root_install/bin/thisroot.sh


# Set helpful environment variables to point to local ROOT installation
ENV ROOTSYS /opt/app-root/src/root_install
ENV PATH="${PATH}:${ROOTSYS}/bin"
ENV CMAKE_PREFIX_PATH=/opt/app-root/src/root_install
ENV DYLD_LIBRARY_PATH=/opt/app-root/src/root_install/lib
ENV JUPYTER_PATH=/opt/app-root/src/root_install/etc/notebook
ENV LD_LIBRARY_PATH=/opt/app-root/src/root_install/lib
ENV LIBPATH=/opt/app-root/src/root_install/lib
ENV PYTHONPATH=/opt/app-root/src/root_install/lib
ENV SHLIB_PATH=/opt/app-root/src/root_install/lib

RUN useradd -m -u 1000 -o -s /bin/bash user
USER user
WORKDIR /home/user
